import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import {
  MatCardModule,
  MatInputModule,
  MatIconModule,
  MatButtonModule,
  MatTableModule,
} from "@angular/material";
import { PokemonSearchInputComponent } from "./pokemon-search-input/pokemon-search-input.component";
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { PokemonCardComponent } from "./pokemon-card/pokemon-card.component";
import { PokemonNotFoundComponent } from "./pokemon-not-found/pokemon-not-found.component";
import { PokemonSavedTableComponent } from "./pokemon-saved-table/pokemon-saved-table.component";

@NgModule({
  declarations: [
    AppComponent,
    PokemonSearchInputComponent,
    PokemonCardComponent,
    PokemonNotFoundComponent,
    PokemonSavedTableComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatTableModule,
    FormsModule,
    MatCardModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
