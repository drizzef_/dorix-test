import { Component, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "app-pokemon-search-input",
  templateUrl: "./pokemon-search-input.component.html",
  styleUrls: ["./pokemon-search-input.component.css"],
})
export class PokemonSearchInputComponent {
  @Output() onSearchEvent = new EventEmitter<string>();
  value = "";

  onEnter(value: string) {
    this.onSearchEvent.emit(value);
  }
}
