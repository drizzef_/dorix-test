import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PokemonSavedTableComponent } from './pokemon-saved-table.component';

describe('PokemonSavedTableComponent', () => {
  let component: PokemonSavedTableComponent;
  let fixture: ComponentFixture<PokemonSavedTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PokemonSavedTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PokemonSavedTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
