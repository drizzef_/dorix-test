import { Component, OnInit, Input } from "@angular/core";

export interface PokemonHistoryRecord {
  name: string;
  image: string;
}

@Component({
  selector: "app-pokemon-saved-table",
  templateUrl: "./pokemon-saved-table.component.html",
  styleUrls: ["./pokemon-saved-table.component.css"],
})
export class PokemonSavedTableComponent {
  @Input() history: PokemonHistoryRecord[];
  displayedColumns: string[] = ["image", "name"];

  constructor() {}
}
