import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "../environments/environment";
import { PokemonCard } from "./pokemon-card/pokemon-card.component";
import { Observable } from "rxjs";
import { PokemonHistoryRecord } from "./pokemon-saved-table/pokemon-saved-table.component";

export interface PokemonCardResponse {
  data: PokemonCard;
  error?: {
    msg: string;
  };
}

export interface PokemonSavedHistoryResponse {
  data: Array<PokemonHistoryRecord>;
  error?: {
    msg: string;
  };
}

@Injectable({
  providedIn: "root",
})
export class PokemonService {
  private baseURL: string;

  constructor(private readonly http: HttpClient) {
    this.baseURL = environment.apiURI;
  }

  getPokemon(name: string): Observable<PokemonCardResponse> {
    return this.http.get<PokemonCardResponse>(
      `${this.baseURL}/pokemons/${name}`
    );
  }

  getPokemonSavedHistory(): Observable<PokemonSavedHistoryResponse> {
    return this.http.get<PokemonSavedHistoryResponse>(
      `${this.baseURL}/pokemons/saved`
    );
  }

  savePokemon(pokemonHistoryRecord: PokemonHistoryRecord) {
    return this.http.post(`${this.baseURL}/pokemons`, pokemonHistoryRecord);
  }
}
