import { Component, OnInit } from "@angular/core";
import { PokemonService, PokemonCardResponse } from "./pokemon.service";
import { PokemonCard } from "./pokemon-card/pokemon-card.component";
import { PokemonHistoryRecord } from "./pokemon-saved-table/pokemon-saved-table.component";
@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent implements OnInit {
  pokemon: PokemonCard;
  isNotFound: boolean;
  pokemonHistory: PokemonHistoryRecord[];
  isPokemonAlreadySaved: boolean;

  constructor(private readonly pokemonService: PokemonService) {}

  ngOnInit() {
    /**
     * Get pokemon history
     */
    this.loadHistory();
  }

  savePokemonToHistory(pokemon: PokemonHistoryRecord) {
    this.pokemonService.savePokemon(pokemon).subscribe(async () => {
      await this.loadHistory();
      this.isPokemonAlreadySaved = this.isPokemonSaved(pokemon.name);
    });
  }

  onSearchInputEnter(name: string) {
    /**
     * Clean up
     */
    this.isNotFound = false;
    this.pokemon = null;
    name = name.toLowerCase();
    this.pokemonService.getPokemon(name).subscribe((res) => {
      if (res.error) this.isNotFound = true;
      if (res.data) {
        this.isPokemonAlreadySaved = this.isPokemonSaved(name);
        this.pokemon = res.data;
      }
    });
  }

  private isPokemonSaved(name: string) {
    return this.pokemonHistory.some((x) => x.name === name);
  }

  private loadHistory() {
    return new Promise((resolve) => {
      this.pokemonService.getPokemonSavedHistory().subscribe((res) => {
        this.pokemonHistory = res.data;
        resolve(res.data);
      });
    });
  }
}
