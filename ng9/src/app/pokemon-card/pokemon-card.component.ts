import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

export interface PokemonCard {
  name: string;
  image: string;
  abilities: string[];
  types: string[];
}

@Component({
  selector: "app-pokemon-card",
  templateUrl: "./pokemon-card.component.html",
  styleUrls: ["./pokemon-card.component.css"],
})
export class PokemonCardComponent {
  @Input() pokemon: PokemonCard;
  @Input() isSaved: boolean;
  @Output() onSaveEvent = new EventEmitter<PokemonCard>();
  isSaving: boolean;

  constructor() {}

  onSaveBtnClicked() {
    this.isSaving = true;
    this.onSaveEvent.emit(this.pokemon);
    setTimeout(() => (this.isSaving = false), 1000);
  }
}
