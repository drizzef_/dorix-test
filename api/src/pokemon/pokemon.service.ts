import { Model } from 'mongoose';
import { Injectable, Inject } from '@nestjs/common';
import { Pokemon } from './pokemon.interface';
import { CreatePokemonDto } from './pokemon.dto';

@Injectable()
export class PokemonService {
  constructor(
    @Inject('POKEMON_MODEL')
    private pokemonModel: Model<Pokemon>,
  ) {}

  async create(createPokemonDto: CreatePokemonDto): Promise<Pokemon> {
    const createdPokemon = new this.pokemonModel(createPokemonDto);
    return createdPokemon.save();
  }

  async getAll(): Promise<Pokemon[]> {
    return this.pokemonModel.find();
  }
}
