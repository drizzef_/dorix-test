import { Controller, Post, Body, Get, Param } from '@nestjs/common';
import { CreatePokemonDto } from './pokemon.dto';
import { PokemonService } from './pokemon.service';
import { PokeAPIService } from '../pokeapi/pokeapi.service';
import { UploaderService } from 'src/uploader/uploader.service';

@Controller('pokemons')
export class PokemonController {
  constructor(
    private readonly pokemonService: PokemonService,
    private readonly pokeApiService: PokeAPIService,
    private readonly uploaderService: UploaderService,
  ) {}

  @Post('/')
  async create(@Body() createPokemonDto: CreatePokemonDto) {
    // upload to S3
    const s3Path = await this.uploaderService.upload(createPokemonDto.image);
    createPokemonDto.image = s3Path;

    // save to db
    return this.pokemonService.create(createPokemonDto);
  }

  @Get('/saved')
  async getSavedHistory() {
    const result = await this.pokemonService.getAll();
    return {
      data: result,
    };
  }

  @Get('/:name')
  async getPokemon(@Param('name') name: string) {
    try {
      const result = await this.pokeApiService.findOne(name);
      return {
        data: {
          name: result.name,
          image: result.sprites.front_default,
          abilities: result.abilities.map(result => result.ability.name),
          types: result.abilities
            .filter(x => x === null)
            .map(result => result.type?.name),
        },
      };
    } catch (error) {
      return {
        error: {
          msg: `There is no pokemon named ${name}`,
        },
      };
    }
  }
}
