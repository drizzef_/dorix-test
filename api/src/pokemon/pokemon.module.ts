import { Module } from '@nestjs/common';
import { DatabaseModule } from '../database/database.module';
import { PokemonController } from './pokemon.controller';
import { pokemonProviders } from './pokemon.providers';
import { PokemonService } from './pokemon.service';
import { PokeapiModule } from '../pokeapi/pokeapi.module';
import { UploaderModule } from '../uploader/uploader.module';

@Module({
  imports: [DatabaseModule, PokeapiModule, UploaderModule],
  controllers: [PokemonController],
  providers: [PokemonService, ...pokemonProviders],
})
export class PokemonModule {}
