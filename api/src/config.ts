export default {
  db: {
    connectionString: process.env.DB_URI || `mongodb://dorix@127.0.0.1/pokemon`,
  },
  aws: {
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  },
};
