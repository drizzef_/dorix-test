import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PokemonModule } from './pokemon/pokemon.module';
import { PokeapiModule } from './pokeapi/pokeapi.module';
import { UploaderModule } from './uploader/uploader.module';

@Module({
  imports: [PokemonModule, PokeapiModule, UploaderModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
