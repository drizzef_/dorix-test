import { Injectable } from '@nestjs/common';
import * as request from 'request';
import { S3 } from 'aws-sdk';
import { v4 as uuidv4 } from 'uuid';
import config from '../config';

@Injectable()
export class UploaderService {
  private s3Client: S3;
  constructor() {
    this.s3Client = new S3({
      region: 'eu-central-1',
      apiVersion: '2006-03-01',
      accessKeyId: config.aws.accessKeyId,
      secretAccessKey: config.aws.secretAccessKey,
    });
  }
  public async upload(imageURL: string) {
    const buffer = await this.getImgBuffer(imageURL);
    const { Location } = await this.s3Client
      .upload({
        Bucket: 'tamirtests',
        Key: `images/${this.generateFilename()}`,
        Body: buffer,
        ContentType: 'image/jpeg',
        ACL: 'public-read',
      })
      .promise();
    return Location;
  }

  private async getImgBuffer(imgUrl: string): Promise<Buffer> {
    return new Promise((resolve, reject) => {
      const userAgent =
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.1.3239.108 WorkStream/63.1.3239.108 Safari/537.36';
      const defaultProtocol = 'https:';
      /**
       * Mimic HSTS like the browser
       * @see https://superuser.com/questions/565409/how-to-stop-an-automatic-redirect-from-http-to-https-in-chrome
       */
      if (imgUrl.match(/^\/\/.*/) !== null) {
        imgUrl = defaultProtocol + imgUrl;
      }

      const imgParsedUrl = new URL(imgUrl);
      imgParsedUrl.protocol = defaultProtocol;

      const headers = {
        'User-Agent': userAgent,
      };

      request.get(
        imgParsedUrl.href,
        { encoding: null, gzip: true, headers },
        (error, response, body) => {
          if (error) return reject(error);
          return resolve(body);
        },
      );
    });
  }

  private generateFilename() {
    return `${uuidv4()}${new Date().getTime()}.png`;
  }
}
