import { Module } from '@nestjs/common';
import { PokeAPIService } from './pokeapi.service';

@Module({
  providers: [PokeAPIService],
  exports: [PokeAPIService],
})
export class PokeapiModule {}
