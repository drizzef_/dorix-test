import { Injectable } from '@nestjs/common';
import Axios, { AxiosInstance } from 'axios';
@Injectable()
export class PokeAPIService {
  private readonly BASE_PATH: string = 'https://pokeapi.co/api/v2';

  private client: AxiosInstance;

  constructor() {
    this.client = Axios.create({
      baseURL: this.BASE_PATH,
    });
  }

  async findSome(offset: number = 0, limit: number = 100) {
    return this.client
      .get(`/pokemon?limit=${limit}&offset=${offset}`)
      .then(x => x.data);
  }

  async findOne(name: string) {
    return this.client.get(`/pokemon/${name}`).then(x => {
      return x.data;
    });
  }
}
