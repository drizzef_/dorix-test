module.exports = {
  apps: [
    {
      name: 'dorix_api',
      script: './dist/main.js',
      watch: true,
      env_production: {
        PORT: 8080,
        NODE_ENV: 'production',
        DB_URI: 'mongodb://dorix@127.0.0.1/pokemon',
      },
    },
  ],
};
